## 每日健康打卡部署说明
下载代码
```
    git clone https://github.com/windrunner0/HealthyInfo.git
```
下载微信开发者工具
```
    https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html
```

选择最新稳定版下载
```
	将文件目录导入微信开发者工具
```
参数修改
```
    修改project.config.json中的appid为自己的appid
    修改云函数cloudfunctions/sendclickmsg/index.js 中的templateId为自己小程序后台的templateId
```
部署云函数
```
    msgSC（敏感字符检测）、sendclickmsg(每日健康打卡消息推送)
```
创建云数据库

表名 | 含义 | 权限
---|---|---
EPIDEMICINFO | 打卡记录集合 | 所有用户可读，仅创建者及管理员可写
PUSER | 用户信息设置集合 | 仅创建者及管理员可读写
ADVICE | 用户意见反馈集合 | 所有用户可读，仅创建者及管理员可写
CLOCKTASK | 用户订阅提醒集合 | 所有用户可读，仅创建者及管理员可写
ORGANIZATION | 群组 | 所有用户可读，仅创建者及管理员可写
USERORG | 用户群组 | 所有用户可读，仅创建者及管理员可写
USERORGINFO | 用户群组签到打卡 | 所有用户可读，仅创建者及管理员可写

编译、预览、测试 

联系方式:微信：zhengpeng0725


## 开发说明

  有意参与本项目的伙伴，欢迎fork代码，并提交Pr


## LICENSE
  本项目采用开源协议： Apache License Version 2.0