# 每日健康打卡

健康监督打卡是极客君在2019-nCoV期间推出的一款身体状态打卡企业级微信小程序工具，目的在于为企业、小区、社区、家庭等圈子提供线上健康信息收集

## 特性
1. 每日健康打卡
1. 订阅打卡通知
1. 个人信息设置
1. 群组创建
1. 意见反馈
1. 加入群组 

## 依赖

- 小程序.云开发环境 云数据库 云存储 

## 部署说明
请移步：
```
    https://gitee.com/1150758691/tcb_hackthon_epidemicInfo/blob/master/deployment.md
```

## Bug 反馈

  如果有 Bug ，请通过 微信：zhengpeng0725 反馈

## 联系方式
  微信：zhengpeng0725

有任何问题，可以通过下方的联系方式联系我。

## 版本更新
请移步：
```
    https://gitee.com/1150758691/tcb_hackthon_epidemicInfo/blob/master/changelog.md
```

## 团队信息

- HMY极客小队
