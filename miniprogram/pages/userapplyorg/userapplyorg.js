// pages/databaseGuide/databaseGuide.js

const app = getApp()

Page({
  
  data: {

  },

  onLoad: function (options) {
    let that = this;
    that.orgid = options.orgid
    console.log("进入群组申请列表页面，查询条件orgid为：" + options.orgid)
    this.onQuery((this.data.page));
  },


  QryOrgDetailpre: function (e) {
    console.log("列表跳转详情页参数中_id值为：" + e.currentTarget.dataset.id._id)
    
    var orgdata = JSON.stringify(e.currentTarget.dataset.id)
    console.log("列表跳转详情页参数:" + orgdata);
    wx.navigateTo({
      url: '../orgdetail/orgdetail?orgdata=' + orgdata,
    })
  },

  onPullDownRefresh: function () {
    if (this.data.isRefreshing || this.data.isLoadingMoreData) {
      return
    }
    this.setData({
      isRefreshing: true,
      hasMoreData: true
    })
    this.onQuery(this.data.page);
  },

  refuse: function (e) {
    let that = this
    var value = e.currentTarget.dataset.id
    console.log("用户审批结果为：" + value)

    wx.cloud.callFunction({
      // 云函数名称
      name: 'authapplyorg',
      // 传给云函数的参数
      data: {
        authstatus: "2",
        applyid: value
      },
      success: function (res) {
        console.log(res)
        this.onQuery(that.data.page);
      },
      fail: console.error
    })
    },


  agree: function (e) {
    let that = this
    var value = e.currentTarget.dataset.id
    console.log("用户审批结果为：" + value)

    wx.cloud.callFunction({
      // 云函数名称
      name: 'authapplyorg',
      // 传给云函数的参数
      data: {
        authstatus: "1",
        applyid: value
      },
      success: function (res) {
        console.log(res)
        that.onQuery(that.data.page);
      },
      fail: console.error
    })
  },

  onQuery: function () {
    if (app.globalData.nickName == null) {
      wx.navigateTo({
        url: '../login/login',
      })
      return;
    }

    const db = wx.cloud.database();
    var that = this;
    // 获取总数
    db.collection('USERORG').where({
      orgid:that.orgid
    }).count({
      success: function (res) {
        console.log("群组分页查询记录数为：" + res.total)
        that.data.totalCount = res.total;
      }
    })

    console.log("查询群组ID为：" + that.orgid)
    db.collection('USERORG').where({
      orgid: that.orgid
    }).limit(10) // 限制返回数量为 10 条
      .orderBy('addtime', 'desc').get({
      success: res => {
        console.log("群组申请记录查询结果：" + JSON.stringify(res.data));
        that.data.orglistdata = res.data;
        this.setData({
          orglistdata: that.data.orglistdata
        })
        wx.hideLoading();
        wx.hideNavigationBarLoading();//隐藏加载
        wx.stopPullDownRefresh();
        console.log('[数据库] [查询记录] 成功: ', res)
      },
      fail: err => {
        wx.hideLoading();
        wx.stopPullDownRefresh();
        console.error('[数据库] [查询记录] 失败：', err)
      }
    })
  },

  /**
 * 分页处理函数
 */
  onReachBottom: function () {
    var that = this;
    var temp = [];
    // 获取后面十条
    if (this.data.orglistdata.length < this.data.totalCount) {
      console.log("分页查询，当前skip的值为：" + this.data.orglistdata.length);
      try {
        const db = wx.cloud.database();
        db.collection('USERORG').where({
          applystatus: '0',    //0待审核
          orgid: that.orgid
        }).skip(this.data.orglistdata.length)
          .limit(10) // 限制返回数量为 10 条
          .orderBy('addtime', 'desc') // 排序
          .get({
            success: function (res) {
              // res.data 是包含以上定义的两条记录的数组
              console.log(res.data);
              if (res.data.length > 0) {
                for (var i = 0; i < res.data.length; i++) {
                  var tempTopic = res.data[i];
                  temp.push(tempTopic);
                }
                var totalactivies = {};
                totalactivies = that.data.orglistdata.concat(temp);
                that.setData({
                  orglistdata: totalactivies,
                })
              } else {
                wx.showToast({
                  title: '没有更多数据了',
                })
              }
            },
            fail: function (event) {
              console.log("======" + event);
            }
          })
      } catch (e) {
        console.error(e);
      }
    } else {
      wx.showToast({
        title: '没有更多数据了',
      })
    }
  }
})