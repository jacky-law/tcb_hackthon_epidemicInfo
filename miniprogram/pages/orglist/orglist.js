// pages/databaseGuide/databaseGuide.js

const app = getApp()

Page({
  
  data: {

  },

  onLoad: function (options) {
    this.onQueryHot();
    this.onQuery((this.data.page));
  },

  QryOrgDetailpre: function (e) {
    console.log("列表跳转详情页参数中_id值为：" + e.currentTarget.dataset.id._id)  
    var orgdata = JSON.stringify(e.currentTarget.dataset.id)
    console.log("列表跳转详情页参数:" + orgdata);
    wx.navigateTo({
      url: '../orgdetail/orgdetail?orgdata=' + orgdata,
    })
  },

  QryOrgDetail: function (e) {
    if (app.globalData.nickName == null) {
      wx.navigateTo({
        url: '../login/login',
      })
      return;
    }

    console.log("列表跳转详情页参数中_id值为：" + e.currentTarget.dataset.id._id)
    var userorgdata = JSON.stringify(e.currentTarget.dataset.id)
    console.log("列表跳转详情页参数:" + userorgdata);
    wx.navigateTo({
      url: '../userorgdetail/userorgdetail?userorgdata=' + userorgdata,
    })
  },

  onPullDownRefresh: function () {
    if (this.data.isRefreshing || this.data.isLoadingMoreData) {
      return
    }
    this.setData({
      isRefreshing: true,
      hasMoreData: true
    })
    this.onQueryHot();
    this.onQuery(this.data.page);
  },

  //查询热门圈子
  onQueryHot: function () {
    const db = wx.cloud.database();
    var that = this;
    db.collection('ORGANIZATION').where({
      orgstatus: '1',    //0待审核
      hotflag:'1'
    }).limit(10) // 限制返回数量为 10 条
      .orderBy('hotid', 'desc').get({
        success: res => {
          console.log("热门群组列表查询成功：" + JSON.stringify(res.data));
          that.setData({
            hotorglistdata: res.data
          })
          wx.hideLoading();
          wx.hideNavigationBarLoading();//隐藏加载
          wx.stopPullDownRefresh();
          console.log('[数据库] [查询记录] 成功: ', res)
        },
        fail: err => {
          wx.hideLoading();
          wx.stopPullDownRefresh();
          console.error('[数据库] [查询记录] 失败：', err)
        }
      })
  },

  onQuery: function () {
    const db = wx.cloud.database();
    var that = this;
    // 获取总数
    db.collection('ORGANIZATION').where({
      orgstatus: '1',    //0待审核
    }).count({
      success: function (res) {
        console.log("群组分页查询记录数为：" + res.total)
        that.data.totalCount = res.total;
      }
    })

    db.collection('ORGANIZATION').where({
      orgstatus: '1'    //0待审核
    }).limit(10) // 限制返回数量为 10 条
      .orderBy('addtime', 'desc').get({
      success: res => {
        console.log("群组列表查询成功：" + res.data);
        console.log("群组列表查询成功：" + JSON.stringify(res.data));

        that.data.orglistdata = res.data;
        this.setData({
          orglistdata: that.data.orglistdata
        })
        wx.hideLoading();
        wx.hideNavigationBarLoading();//隐藏加载
        wx.stopPullDownRefresh();
        console.log('[数据库] [查询记录] 成功: ', res)
      },
      fail: err => {
        wx.hideLoading();
        wx.stopPullDownRefresh();
        console.error('[数据库] [查询记录] 失败：', err)
      }
    })
  },

  /**
 * 分页处理函数
 */
  onReachBottom: function () {
    var that = this;
    var temp = [];
    // 获取后面十条
    if (this.data.orglistdata.length < this.data.totalCount) {
      console.log("分页查询，当前skip的值为：" + this.data.orglistdata.length);
      try {
        const db = wx.cloud.database();
        db.collection('ORGANIZATION').where({
          orgstatus: '1'    //0待审核
        }).skip(this.data.orglistdata.length)
          .limit(10) // 限制返回数量为 10 条
          .orderBy('addtime', 'desc') // 排序
          .get({
            success: function (res) {
              // res.data 是包含以上定义的两条记录的数组
              console.log(res.data);
              if (res.data.length > 0) {
                for (var i = 0; i < res.data.length; i++) {
                  var tempTopic = res.data[i];
                  temp.push(tempTopic);
                }
                var totalactivies = {};
                totalactivies = that.data.orglistdata.concat(temp);
                that.setData({
                  orglistdata: totalactivies,
                })
              } else {
                wx.showToast({
                  title: '没有更多数据了',
                })
              }
            },
            fail: function (event) {
              console.log("======" + event);
            }
          })
      } catch (e) {
        console.error(e);
      }
    } else {
      wx.showToast({
        title: '没有更多数据了',
      })
    }
  }
})