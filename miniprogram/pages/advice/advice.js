// pages/databaseGuide/databaseGuide.js

const app = getApp()

Page({

  data: {
  },

  onLoad: function (options) {
    this.setData({
      avatarUrl: app.globalData.avatarUrl,
      nickName: app.globalData.nickName,
    })
  },

  onAdd: function (e) {
    var nickName = app.globalData.nickName;
    const db = wx.cloud.database()
    db.collection('ADVICE').add({
       data: {
         nickName: nickName,
         company: e.detail.value.company,
         advice: e.detail.value.advice
       },
       success: res => {
         wx.reLaunch({
           url: '../msg/msg_success',
         })
         console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
       },
       fail: err => {
         wx.reLaunch({
           url: '../msg/msg_fail',
         })
         console.error('[数据库] [新增记录] 失败：', err)
       }
     })
  },

  goHome: function() {
    const pages = getCurrentPages()
    if (pages.length === 2) {
      wx.navigateBack()
    } else if (pages.length === 1) {
      wx.redirectTo({
        url: '../index/index',
      })
    } else {
      wx.reLaunch({
        url: '../index/index',
      })
    }
  }
})