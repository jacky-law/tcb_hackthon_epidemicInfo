// pages/databaseGuide/databaseGuide.js

const app = getApp()

Page({
  
  data: {
    openid: '',
    nickName: '',
    queryResult: '',
    temprateData: [{ "id": 1, "name": "<36℃" }, { "id": 2, "name": "36℃-37℃" }, { "id": 3, "name": "37℃-38℃" }, { "id": 4, "name": ">38℃" }],
    selectData: [{ "id": 1, "name": "一切正常" }, { "id": 2, "name": "发热" }, { "id": 3, "name": "干咳" }, { "id": 4, "name": "腹泻" }, { "id": 5, "name": "感冒" }, { "id": 6, "name": "头疼头晕" }],
    imgbox: [],//选择图片
    fileIDs: []//上传云存储后的返回值
  },

  onLoad: function (options) {
    var userclickdata = JSON.parse(options.userclickdata);
    console.log("进入打卡页面参数：" + userclickdata)
    console.log("userclickdata:" + JSON.stringify(userclickdata, null, 2));
    app.globalData.orgdata = userclickdata
    var date = new Date();
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '';
    this.setData({
      date: Y + M + D
    })
    if (app.globalData.openid) {
      this.setData({
        openid: app.globalData.openid,
        nickName: app.globalData.nickName,
        avatarUrl: app.globalData.avatarUrl
      })
    }
  },

  checkboxChange: function (e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
    console.log("长度:" + e.detail.value.length);
    app.globalData.status = e.detail.value
    this.setData({
      id: e.detail.value,
      length: e.detail.value.length
    })
  },

  radioChange: function (e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
    console.log("长度:" + e.detail.value.length);
    app.globalData.temperature = e.detail.value
    this.setData({
      id: e.detail.value,
      length: e.detail.value.length
    })
  },

  onAdd: function (e) {
    if (app.globalData.nickName == null) {
      wx.navigateTo({
        url: '../login/login',
      })
      return;
    }
    wx.showLoading({
      title: '数据提交中',
    })

    if (app.globalData.temperature == null || app.globalData.temperature == ''){
        wx.showToast({
          icon: 'none',
          title: '温度不能为空'
        })
        return false
    }

    if (e.detail.value.date == null || e.detail.value.date == '') {
      wx.showToast({
        icon: 'none',
        title: '日期不能为空'
      })
      return false
    }

    if (app.globalData.status == null || app.globalData.status == '') {
      wx.showToast({
        icon: 'none',
        title: '身体状态不能为空'
      })
      return false
    }

    const db = wx.cloud.database()
    var date = new Date();
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '  ';
    var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var date = date.getDate();
    console.log("当前用户昵称：" + app.globalData.nickName);
    console.log("当前用户头像路径：" + app.globalData.avatarUrl);
    console.log("当前时间：" + Y + M + D + h + m + s);
    console.log("新增姓名为：" + e.detail.value.name);
    console.log("新增手机号为：" + e.detail.value.phone);
    console.log("新增在京住址为：" + e.detail.value.address);
    console.log("新增学校为：" + e.detail.value.company);
    console.log("新增体温为：" + e.detail.value.temperature);
    console.log("用户状态为：" + app.globalData.status);
    
    console.log("新增时间为：" + e.detail.value.date + " : " + e.detail.value.time);
    db.collection('USERORGINFO').add({
      data: {
        name: e.detail.value.name,
        nickname: app.globalData.nickName,
        avatarurl: app.globalData.avatarUrl,
        phone: e.detail.value.phone,
        address: e.detail.value.address,
        company: e.detail.value.company,
        temperature: app.globalData.temperature,
        date: e.detail.value.date,
        time: e.detail.value.time,
        remark: e.detail.value.remark,
        status:app.globalData.status,
        currenttime: Y + M + D + h + m + s,
        orgdataid: app.globalData.orgdata._id,
        applystatus : '0',
        orgdata: app.globalData.orgdata
      },
      success: res => {
        wx.hideLoading()
        this.setData({
          contentStr:''
        })
        console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
        app.globalData.status = null
        wx.reLaunch({
          url: '../msg/msg_success',
        })
      },
      fail: err => {
        app.globalData.status=null
        app.globalData.headurl = null;
        wx.hideLoading()
        wx.reLaunch({
          url: '../msg/msg_fail',
        })
        console.error('[数据库] [新增记录] 失败：', err)
      }
    })
  },

  onPullDownRefresh: function () {
    this.updateBlogs()
  },
  updateBlogs: function () {
    this.onQuery()
  },

  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },

  onQuery: function () {
    wx.showLoading({
      title: '数据加载中',
    })
    const db = wx.cloud.database()
    //查询当前用户所有的 counters
    db.collection('PVERSION').where({
        version: 2019
    }).orderBy('time', 'desc').get({
      success: res => {
        this.setData({
          queryResult: res.data
        })
        console.log('[数据库] [查询记录] 成功: ', res)
        wx.hideLoading()
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
        console.error('[数据库] [查询记录] 失败：', err)
        wx.hideLoading()
      }
    })
  },

  goHome: function() {
    const pages = getCurrentPages()
    if (pages.length === 2) {
      wx.navigateBack()
    } else if (pages.length === 1) {
      wx.redirectTo({
        url: '../index/index',
      })
    } else {
      wx.reLaunch({
        url: '../index/index',
      })
    }
  }

})