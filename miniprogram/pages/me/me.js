// pages/databaseGuide/databaseGuide.js

const app = getApp()

Page({

  data: {
    userInfoFlagYes: true,
    userInfoFlagNo: false,
  },

  onLoad: function (options) {
    this.onGetOpenid();
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              app.globalData.nickName = res.userInfo.nickName
              app.globalData.avatarUrl = res.userInfo.avatarUrl
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                nickName: res.userInfo.nickName,
                userInfo: res.userInfo
              })
            }
          })
        } else {
          this.setData({
            userInfoFlagYes: false,
            userInfoFlagNo: true
          })
        }
      }
    })

    if (app.globalData.openid) {
      this.setData({
        openid: app.globalData.openid
      })
    }
  },

  
  onGetOpenid: function () {
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        app.globalData.openid = res.result.openid
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
      }
    })
  },

  onCheckAuth: function(e) {
    if (app.globalData.nickName == null) {
      wx.showToast({
        title: '【请授权登录】',
        icon: "none"
      })
      return;
    }
    wx.navigateTo({
      url: '../setting/setting',
    })
  },

  openGallery: function () {
    this.setData({
      istrue: true
    })
  },
  closeGallery: function () {
    this.setData({
      istrue: false
    })
  },

  onGetUserInfo: function (e) {
    let that = this;
    // 获取用户信息
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          console.log("已授权=====")
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success(res) {

              console.log("获取用户信息成功", res)
              app.globalData.nickName = res.userInfo.nickName
              app.globalData.avatarUrl = res.userInfo.avatarUrl
              that.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo,
                userInfoFlagYes: true,
                userInfoFlagNo: false
              })
              wx.reLaunch({
                url: 'index',
              })
            },
            fail(res) {
              console.log("获取用户信息失败", res)
            }
          })
        } else {
          console.log("未授权=====")
        }
      }
    })
  },

  onWaiting: function (e) {
    wx.showToast({
      icon: 'none',
      title: '开发中，敬请期待!'
    })
  },  

  goHome: function() {
    const pages = getCurrentPages()
    if (pages.length === 2) {
      wx.navigateBack()
    } else if (pages.length === 1) {
      wx.redirectTo({
        url: '../index/index',
      })
    } else {
      wx.reLaunch({
        url: '../index/index',
      })
    }
  }

})