const app = getApp()

Page({
  
  data: {

  },

  onLoad: function (options) {
    this.onQuery();
  },

  onQuery: function () {
    const db = wx.cloud.database()
    console.log("打印我的提交记录，查询id：" + app.globalData.openid);
    if (app.globalData.openid == null || app.globalData.openid == ''){
        return;
    }
    db.collection('EPIDEMICINFO').where({
      _openid: app.globalData.openid
    }).get({
      success: res => {
        console.log("查询我的报名列表成功：" + res.data);
        this.setData({
          queryResult: res.data
        })
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
        console.error('[数据库] [查询记录] 失败：', err)
      }
    })
  }
})