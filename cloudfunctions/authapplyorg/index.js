const cloud = require('wx-server-sdk')
//环境变量 ID
cloud.init()

const db = cloud.database()
// 云函数入口函数
//传递的参数可通过event.xxx得到
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  try {
    return await db.collection('USERORG').doc(event.applyid).update({
      data: {
        _openid: wxContext.OPENID,
        applystatus: event.authstatus
      }
    })
  } catch (e) {
    console.error(e)
  }
}