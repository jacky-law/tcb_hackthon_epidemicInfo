const cloud = require('wx-server-sdk')
cloud.init()
let templateId = 'oCYqcR_nT5B4qZs3MuaylO4SqsdkErnSTMvYyee8K1g';

async function getAdviceTask(cdate) {
  try {
    const result = await cloud.openapi.subscribeMessage.send({
      touser: cdate_openid,
      page: 'index',
      data: {
        thing4: {
          value: cdate.nickName + '每日健康打卡'
        },
        thing5: {
          value: "武汉加油、中国加油"
        },
        thing3: {
          value: "抗击疫情、我们同在！"
        }
      },
      templateId: templateId
    })
    console.log(result)
  } catch (err) {
    console.log(err)
  }
}

const db = cloud.database()
exports.main = async (event, context) => {
  console.log("开始查询并处理打卡任务列表......");
  let cdata = await db.collection('CLOCKTASK').where({

  }).get()
  console.log(cdata);
  let taskData = cdata.data
  for (var i = 0; i < taskData.length; i++) {
    console.log("处理任务任务_openid......")
    try {
      const result = await cloud.openapi.subscribeMessage.send({
        touser: taskData[i]._openid,
        page: 'pages/index/index',
        data: {
          thing4: {
            value: '每日健康打卡'
          },
          thing5: {
            value: "武汉加油、中国加油"
          },
          thing3: {
            value: "抗击疫情、我们同在！"
          }
        },
        templateId: templateId
      })
      console.log(result)
    } catch (err) {
      console.log(err)
    }
  }
}